# Introduction

Hello, example work with text generation in flask-microservice app

# run docker

### build and run
docker-compose up --build

# request

### write some word on russion
http://localhost:5001/ - enter the text

example:

![Example](/static/images/Text generate - example.jpg)

### post request to get text
http://localhost:5001/get_text - can get text by post request

# model
model created by tutorial https://www.tensorflow.org/tutorials/text/text_generation?hl=ru