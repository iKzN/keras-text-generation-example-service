import os
import joblib
import numpy as np
import pandas as pd
import json
import re

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request
from flask_wtf import FlaskForm
from markupsafe import escape
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename

import tensorflow as tf
from tensorflow import keras

app = Flask(__name__)

# Using a development configuration
app.config.from_object('config.DevConfig')

# Build model
json_char = '{" ":0,"!":1,"&":2,"\'":3,"(":4,")":5,",":6,"-":7,".":8,":":9,";":10,"?":11,"а":12,"б":13,"в":14,"г":15,"д":16,"е":17,"ж":18,"з":19,"и":20,"й":21,"к":22,"л":23,"м":24,"н":25,"о":26,"п":27,"р":28,"с":29,"т":30,"у":31,"ф":32,"х":33,"ц":34,"ч":35,"ш":36,"щ":37,"ъ":38,"ы":39,"ь":40,"э":41,"ю":42,"я":43,"ё":44,"–":45,"—":46,"“":47,"„":48,"…":49}'

char2idx = json.loads(json_char)

idx2char = [' ', '!', '&', "'", '(', ')', ',', '-', '.', ':', ';', '?', 'а',
       'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н',
       'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ',
       'ы', 'ь', 'э', 'ю', 'я', 'ё', '–', '—', '“', '„', '…']

def build_model(vocab_size, embedding_dim, rnn_units, batch_size):
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(vocab_size, embedding_dim,
                batch_input_shape=[batch_size, None]),
        tf.keras.layers.GRU(rnn_units,
                return_sequences=True,
                stateful=True,
                recurrent_initializer='glorot_uniform'),
        tf.keras.layers.GRU(rnn_units,
                return_sequences=True,
                stateful=True,
                recurrent_initializer='glorot_uniform'),
        tf.keras.layers.Dense(vocab_size)
    ])
    return model

generation_model = build_model(
    vocab_size = 50,
    embedding_dim=512,
    rnn_units=1024,
    batch_size=1)

generation_model.load_weights("model/test_model_for_docker_weights")
generation_model.build(tf.TensorShape([1, None]))

@app.route('/')
def clear_format():
    return render_template('text_form.html')

@app.route('/get_text', methods = ['POST'])
def load_paint():
    """Return answer"""
    try:
        content = request.get_json()
        predict = generate_text(generation_model, start_string=text_clear(content['input_text']))
    except Exception as e:
        print('error')
        print(e)
        return redirect(url_for('bad_request'))

    return jsonify(predict)

def text_clear(text):
    result = re.sub(r"[A-z]", '', text) 
    result = re.sub(r"[1234567890*+«»@{}©®°ä]", '', result) 
    result = re.sub(r"[\|/]", '', result) 
    result = re.sub(r"[\n]", ' ', result) 
    result = re.sub(r"[\r]", ' ', result) 
    result = re.sub(r'\s+', ' ', result)
    result = result.lower()
    return result

@app.route('/badrequest')
def bad_request():
    return abort(400)

def generate_text(model, start_string):
    # Evaluation step (generating text using the learned model)

    # Number of characters to generate
    num_generate = 100

    # Converting our start string to numbers (vectorizing)
    input_eval = [char2idx[s] for s in start_string]
    input_eval = tf.expand_dims(input_eval, 0)

    # Empty string to store our results
    text_generated = []

    # Low temperatures results in more predictable text.
    # Higher temperatures results in more surprising text.
    # Experiment to find the best setting.
    temperature = 1

    # Here batch size == 1
    model.reset_states()
    for i in range(num_generate):
        predictions = model(input_eval)
        # remove the batch dimension
        predictions = tf.squeeze(predictions, 0)

        # using a categorical distribution to predict the character returned by the model
        predictions = predictions / temperature
        predicted_id = tf.random.categorical(predictions, num_samples=1)[-1,0].numpy()

        # We pass the predicted character as the next input to the model
        # along with the previous hidden state
        input_eval = tf.expand_dims([predicted_id], 0)

        text_generated.append(idx2char[predicted_id])

    return (start_string + ''.join(text_generated))
